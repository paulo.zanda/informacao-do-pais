package devteam.API_Informacoes_Pais.Repository;

import devteam.API_Informacoes_Pais.Models.InformationModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InformationRepository extends JpaRepository<InformationModel, Integer> {
}
