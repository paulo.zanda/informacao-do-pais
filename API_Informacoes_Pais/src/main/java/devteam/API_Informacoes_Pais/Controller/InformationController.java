package devteam.API_Informacoes_Pais.Controller;

import devteam.API_Informacoes_Pais.Models.InformationModel;
import devteam.API_Informacoes_Pais.Repository.InformationRepository;
//import org.hibernate.annotations.GeneratorType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping({"/information"})
public class InformationController {
    @Autowired
    private InformationRepository info;
    private InformationModel inf01;

    public InformationController (InformationRepository info){
        this.info = info;
    }

    @GetMapping(path = "/api/information/findID/{id}")
    public ResponseEntity findID (@PathVariable("id") Integer id){
        return info.findById(id)
                .map(record -> ResponseEntity.ok().body(record))
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping (path = "/api/information/save")
    public InformationModel save(@RequestBody InformationModel im){ return info.save(im);}

    @DeleteMapping(path = "/api/information/deleteID/{id}")
    public ResponseEntity <?>  deleteID (@PathVariable("id") Integer id){
        /*
        * ESTE METODO DEVE ELIMINAR UM ELEMENTO DA BASE DE DADOS APARTIR DO NOME*/
        return this.info.findById(id)
                .map(record ->{
                    this.info.deleteById(id);
                    return ResponseEntity.ok().build();
                })
                .orElse(ResponseEntity.notFound().build());
    }

    @PutMapping (value = "/api/information/updateID/{id}")
    public ResponseEntity updateID(@PathVariable("id") Integer id, @RequestBody InformationModel info){
        return this.info.findById(id)
                .map(record ->{
                    record.setNome(info.getNome());
                    record.setArea(info.getArea());
                    record.setCapital(info.getCapital());
                    record.setRegiao(info.getRegiao());
                    record.setSubRegiao(info.getSubRegiao());

                    InformationModel updated = this.info.save(record);
                    return ResponseEntity.ok().body(updated);
                })
                .orElse(ResponseEntity.notFound().build());
    }

}
